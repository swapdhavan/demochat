Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "DemoChat"
s.summary = "Bot Chat Robo."
s.requires_arc = true
s.static_framework = true

# 2
s.version = "0.1.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Swapnil Dhavan" => "sdhavan04@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://swapdhavan@bitbucket.org/swapdhavan/demochat"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://swapdhavan@bitbucket.org/swapdhavan/demochat.git",
             :tag => "#{s.version}" }


# 7
s.framework = "UIKit"
s.dependency 'Alamofire', '~> 4.4'
s.dependency 'AlamofireImage', '~> 3.4'
s.dependency 'ISEmojiView'
s.dependency 'GoogleMaps'

# 8
s.source_files = "DemoChat/**/*.{h,m,swift}"

# 9
s.resources = "DemoChat/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "5.0"

end
